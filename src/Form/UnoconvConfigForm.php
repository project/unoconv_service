<?php

namespace Drupal\unoconv_service\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class UnoconvConfigForm.
 */
class UnoconvConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'unoconv_service.unoconvconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unoconv_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('unoconv_service.unoconvconfig');
    $form['binaries_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Binaries Path'),
      '#description' => $this->t('Provides path of binaries on your system. It will be something like /usr/bin/unoconv etc. depending on your unoconv setup.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
      '#default_value' => $config->get('binaries_path'),
    ];
    $form['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('Timeout for unoconv document conversion process.'),
      '#default_value' => $config->get('timeout'),
      '#required' => TRUE,
    ];
    $form['page_range'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page Range'),
      '#description' => $this->t('Provide page range if you want to convert limited amount of pages. Range can be something like &quot;1-14&quot; (without quotes) for pages 1 to 14.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('page_range'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('unoconv_service.unoconvconfig')
      ->set('binaries_path', $form_state->getValue('binaries_path'))
      ->set('timeout', $form_state->getValue('timeout'))
      ->set('page_range', $form_state->getValue('page_range'))
      ->save();
  }

}
