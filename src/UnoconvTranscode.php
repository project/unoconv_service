<?php

namespace Drupal\unoconv_service;

use Alchemy\BinaryDriver\Exception\ExecutionFailureException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystem;
use http\Exception\RuntimeException;
use Unoconv\Unoconv;

/**
 * Class UnoconvTranscode.
 */
class UnoconvTranscode implements UnoconvTranscodeInterface {

  /**
   * Drupal\Core\File\FileSystem definition.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;
  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new UnoconvTranscode object.
   */
  public function __construct(FileSystem $file_system, ConfigFactoryInterface $config_factory) {
    $this->fileSystem = $file_system;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function transcode($source_path, $destination_path, $destination_uri) {
    $unoconv_config = $this->configFactory->get('unoconv_service.unoconvconfig');
    try {
      $unoconv = Unoconv::create(array(
        'timeout' => $unoconv_config->get('timeout'),
        'unoconv.binaries' => $unoconv_config->get('binaries_path'),
      ));
      $unoconv->transcode($source_path, 'pdf', $destination_path);
    } catch (ExecutionFailureException $exception) {
        throw new RuntimeException(
          t('Unoconv failed to transcode file'), $exception->getCode(), $exception);
    }

    // Saving new converted preview pdf file and attach it to media entity.
    $data = file_get_contents($destination_path);
    $preview_file = file_save_data($data, $destination_uri, FILE_EXISTS_RENAME);

    return $preview_file;
  }

}
